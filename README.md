OpenCV camera calibration pattern generator in HTML+JS.

Main repository:

https://gitgud.io/yuvallanger/opencv-calibration-pattern/

Mirrors:

https://gitlab.com/yuvallanger/opencv-calibration-pattern/
